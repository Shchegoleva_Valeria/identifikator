package ru.svs.identifikator;

import java.io.*;
import java.util.regex.*;

/**
 * В данном классе реализовано считывание строки из файла, поиск и удаление комментариев, поиск и удаление
 * пробельных символов, запись строки в новый файл.
 *
 * @author Щеголева В., группа 15ОИТ18
 */
public class Identifikator {
    public static void main(String[] args) throws IOException {
        String string;
        Pattern pattern = Pattern.compile(".*");
        Matcher matcher = pattern.matcher("");

        try (BufferedReader reader = new BufferedReader(new FileReader("//home/lera/IdeaProjects/Identifikator/src/ru/svs/identifikator/Integer.java"));
             BufferedWriter writer = new BufferedWriter(new FileWriter("Integer2.java"))) {
            while ((string = reader.readLine()) != null) {
                matcher.reset(string);
                while (matcher.find()) {
                    string = matcher.group().replaceAll("^\\s*\\/?\\*[^\\/]*[[*]-[/]]?$", "");
                    string = string.replaceAll("\\/\\/.*", "");
                    string = string.replaceAll("\\s+", " ");
                    string = string.replaceAll("Integer", "Integer2");
                    writer.write(string);
                }
            }
        }
    }
}
